// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Limpiar_Las_PlayasTarget : TargetRules
{
	public Limpiar_Las_PlayasTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Limpiar_Las_Playas" } );
	}
}
