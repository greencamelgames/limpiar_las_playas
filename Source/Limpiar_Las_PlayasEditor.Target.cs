// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class Limpiar_Las_PlayasEditorTarget : TargetRules
{
	public Limpiar_Las_PlayasEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Limpiar_Las_Playas" } );
	}
}
