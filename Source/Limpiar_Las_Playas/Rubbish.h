// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Rubbish.generated.h"

UCLASS()
class LIMPIAR_LAS_PLAYAS_API ARubbish : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARubbish();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UPROPERTY(BlueprintReadWrite, Category = "RubbishCountVariables")
		int currentRubbishCount;
	UPROPERTY(BlueprintReadWrite, Category = "RubbishCountVariables")
		int maxRubbishCount;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "CheckRubbishCount")
		bool rubbishCount();
	
};
