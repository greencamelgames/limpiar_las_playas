// Fill out your copyright notice in the Description page of Project Settings.

#include "Rubbish.h"


// Sets default values
ARubbish::ARubbish()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARubbish::BeginPlay()
{
	Super::BeginPlay();

	maxRubbishCount = 30;
	currentRubbishCount = 0;
	
}

// Called every frame
void ARubbish::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ARubbish::rubbishCount()
{
	if (currentRubbishCount >= maxRubbishCount)
	{
		return true;
	}
	else
	{
		return false;
	}
}

