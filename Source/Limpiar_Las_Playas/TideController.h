// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TideController.generated.h"

UCLASS()
class LIMPIAR_LAS_PLAYAS_API ATideController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATideController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
		int highTideHeightInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
		int highTideMaxHeightInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
		int highTideLowHeightInt;

	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
		int lowTideHeightInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
		int lowTideMaxHeightInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
		int lowTideLowHeightInt;

	UFUNCTION(BlueprintCallable, Category = "Bounds")
		int CreateHighTideValue();
	UFUNCTION(BlueprintCallable, Category = "Bounds")
		int CreateLowTideValue();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
