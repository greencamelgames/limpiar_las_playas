// Fill out your copyright notice in the Description page of Project Settings.

#include "RubbishController.h"
#include <iostream>
#include <cstdlib>
#include <ctime>


// Sets default values
ARubbishController::ARubbishController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	lowerBoundXInt = 1;
	lowerBoundYInt = 1;
	higherBoundXInt = 1000;
	higherBoundYInt = 1000;

	rubbishCountInt = 0;

}

// Called when the game starts or when spawned
void ARubbishController::BeginPlay()
{
	Super::BeginPlay();
	
}

int ARubbishController::pickRandomX()
{	
	return (rand()%(higherBoundXInt - lowerBoundXInt + 1) + lowerBoundXInt);
}

int ARubbishController::pickRandomY()
{
	return (rand()%(higherBoundYInt - lowerBoundYInt + 1) + lowerBoundYInt);
}

int ARubbishController::PickRubbishType()
{
	return (rand() % (20 - 1 + 1)+1);
}

// Called every frame
void ARubbishController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

