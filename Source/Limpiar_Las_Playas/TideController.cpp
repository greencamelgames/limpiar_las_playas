// Fill out your copyright notice in the Description page of Project Settings.

#include "TideController.h"

#include <iostream>
#include <cstdlib>
#include <ctime>


// Sets default values
ATideController::ATideController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	highTideHeightInt = 6500;
	highTideMaxHeightInt = 7000;
	highTideLowHeightInt = 6000;

	lowTideHeightInt = 4575;
	lowTideMaxHeightInt = 5000;
	lowTideLowHeightInt = 4500;

}

// Called when the game starts or when spawned
void ATideController::BeginPlay()
{
	Super::BeginPlay();
	
}

int ATideController::CreateHighTideValue()
{
	highTideHeightInt = (rand() % (highTideMaxHeightInt - highTideLowHeightInt + 1) + highTideLowHeightInt);
	return highTideHeightInt;
}

int ATideController::CreateLowTideValue()
{
	lowTideHeightInt = (rand() % (lowTideMaxHeightInt - lowTideLowHeightInt + 1) + lowTideLowHeightInt);
	return lowTideHeightInt;
}

// Called every frame
void ATideController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

