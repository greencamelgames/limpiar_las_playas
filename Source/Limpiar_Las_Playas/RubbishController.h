// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RubbishController.generated.h"

UCLASS()
class LIMPIAR_LAS_PLAYAS_API ARubbishController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARubbishController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
	int lowerBoundXInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
	int lowerBoundYInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
	int higherBoundXInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
	int higherBoundYInt;
	UPROPERTY(BlueprintReadWrite, Category = "Bounds")
	int rubbishCountInt;
	
	UFUNCTION(BlueprintCallable, Category = "Bounds")
		int pickRandomX();
	UFUNCTION(BlueprintCallable, Category = "Bounds")
		int pickRandomY();
	UFUNCTION(BlueprintCallable, Category = "Bounds")
		int PickRubbishType();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	
	
};
